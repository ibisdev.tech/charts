{{- if and (not .Values.application.initializeCommand) .Values.cronjobs -}}
apiVersion: v1
kind: List
items:
{{- range $jobName, $jobConfig:= .Values.cronjobs }}
{{- if $.Capabilities.APIVersions.Has "batch/v1" }}
- apiVersion: "batch/v1"
{{- else }}
- apiVersion: "batch/v1beta1"
{{- end }}
  kind: CronJob
  metadata:
    name: "{{ template "trackableappname" $ }}-{{ $jobName}}"
    annotations:
      {{ if $.Values.gitlab.app }}app.gitlab.com/app: {{ $.Values.gitlab.app | quote }}{{ end }}
      {{ if $.Values.gitlab.env }}app.gitlab.com/env: {{ $.Values.gitlab.env | quote }}{{ end }}
    labels:
      track: "{{ $.Values.application.track }}"
      tier: "{{ $.Values.application.tier }}"
      {{ include "sharedlabels" $ | nindent 6 }}
  spec:
    concurrencyPolicy: {{ default "Forbid" $jobConfig.concurrencyPolicy }}
    failedJobsHistoryLimit: {{ default 1 $jobConfig.failedJobsHistoryLimit }}
    startingDeadlineSeconds: {{ default 300 $jobConfig.startingDeadlineSeconds }}
    schedule: {{ $jobConfig.schedule | quote }}
    {{- if $jobConfig.timeZone }}
    timeZone: {{ $jobConfig.timeZone }}
    {{- end }}
    successfulJobsHistoryLimit: {{ default 1 $jobConfig.successfulJobsHistoryLimit }}
    jobTemplate:
      spec:
        {{- if $jobConfig.activeDeadlineSeconds }}
        activeDeadlineSeconds: {{ $jobConfig.activeDeadlineSeconds }}
        {{- end }}
        {{- if $jobConfig.backoffLimit }}
        backoffLimit: {{ $jobConfig.backoffLimit }}
        {{- end }}
        template:
          metadata:
            annotations:
              checksum/application-secrets: "{{ $.Values.application.secretChecksum }}"
              {{ if $.Values.gitlab.app }}app.gitlab.com/app: {{ $.Values.gitlab.app | quote }}{{ end }}
              {{ if $.Values.gitlab.env }}app.gitlab.com/env: {{ $.Values.gitlab.env | quote }}{{ end }}
              {{- if $.Values.podAnnotations }}
              {{ toYaml $.Values.podAnnotations | nindent 12 }}
              {{- end }}
            labels:
              app: {{ template "appname" $ }}
              release: {{ $.Release.Name }}
              track: "{{ $.Values.application.track }}"
              tier: cronjob
          spec:
            imagePullSecrets:
              {{ toYaml $.Values.image.secrets | nindent 14 }}
            restartPolicy: {{ default "OnFailure" $jobConfig.restartPolicy }}
            {{- with $nodeSelectorConfig := default $.Values.nodeSelector $jobConfig.nodeSelector -}}
            {{- if $nodeSelectorConfig  }}
            nodeSelector:
            {{ toYaml $nodeSelectorConfig | nindent 14 }}
            {{- end }}
            {{- end }}
            {{- with $tolerationsConfig := default $.Values.tolerations $jobConfig.tolerations -}}
            {{- if $tolerationsConfig }}
            tolerations:
            {{ toYaml $tolerationsConfig | nindent 14 }}
            {{- end }}
            {{- end }}
            {{- with $affinityConfig := default $.Values.affinity $jobConfig.affinity -}}
            {{- if $affinityConfig  }}
            affinity:
            {{ toYaml $affinityConfig | nindent 14 }}
            {{- end }}
            {{- end }}
            {{- if $jobConfig.extraVolumes }}
            volumes:
            {{ toYaml $jobConfig.extraVolumes | nindent 14 }}
            {{- end }}
            containers:
            - name: {{ $.Chart.Name }}
              image: "{{ template "cronjobimagename" (dict "job" . "glob" $.Values) }}"
              imagePullPolicy: {{ $.Values.image.pullPolicy }}
              {{- if $jobConfig.command }}
              command:
              {{- range $jobConfig.command }}
              - {{ . }}
              {{- end }}
              {{- end }}
              {{- if $jobConfig.command }}
              args:
              {{- range $jobConfig.args }}
              - {{ . }}
              {{- end }}
              {{- end }}
              {{- if $.Values.application.secretName }}
              envFrom:
              - secretRef:
                  name: {{ $.Values.application.secretName }}
{{- if $jobConfig.extraEnvFrom }}
{{ toYaml $jobConfig.extraEnvFrom | nindent 14 }}
{{- end }}
              {{- else }}
              envFrom:
{{- if $jobConfig.extraEnvFrom }}
{{ toYaml $jobConfig.extraEnvFrom | nindent 14 }}
{{- end }}
              {{- end }}
              env:
              {{- if $.Values.postgresql.managed }}
              - name: POSTGRES_USER
                valueFrom:
                  secretKeyRef:
                    name: app-postgres
                    key: username
              - name: POSTGRES_PASSWORD
                valueFrom:
                  secretKeyRef:
                    name: app-postgres
                    key: password
              - name: POSTGRES_HOST
                valueFrom:
                  secretKeyRef:
                    name: app-postgres
                    key: privateIP
              {{- end }}
              {{- if $.Values.application.database_url }}
              - name: DATABASE_URL
                value: {{ $.Values.application.database_url | quote }}
              {{- end }}
              - name: GITLAB_ENVIRONMENT_NAME
                value: {{ $.Values.gitlab.envName | quote }}
              - name: GITLAB_ENVIRONMENT_URL
                value: {{ $.Values.gitlab.envURL | quote }}
              ports:
              - name: "{{ $.Values.service.name }}"
                containerPort: {{ $.Values.service.internalPort }}
              {{- if $jobConfig.livenessProbe }}
              livenessProbe:
              {{- if eq $jobConfig.livenessProbe.probeType "httpGet" }}
                httpGet:
                  path: {{ $jobConfig.livenessProbe.path }}
                  scheme: {{ $jobConfig.livenessProbe.scheme }}
                  port: {{ default $.Values.service.internalPort $jobConfig.livenessProbe.port }}
              {{- else if eq $jobConfig.livenessProbe.probeType "tcpSocket" }}
                tcpSocket:
                  port: {{ default $.Values.service.internalPort $.Values.service.internalPort }}
              {{- else if eq $jobConfig.livenessProbe.probeType "exec" }}
                exec:
                  command:
                    {{ toYaml $jobConfig.livenessProbe.command | nindent 18 }}
              {{- end }}
                initialDelaySeconds: {{ $jobConfig.livenessProbe.initialDelaySeconds }}
                timeoutSeconds: {{  $jobConfig.livenessProbe.timeoutSeconds }}
                failureThreshold: {{ $jobConfig.livenessProbe.failureThreshold }}
                periodSeconds: {{ $jobConfig.livenessProbe.periodSeconds }}
              {{- end }}
              {{- if $jobConfig.readinessProbe  }}
              readinessProbe:
                {{- if eq $jobConfig.readinessProbe.probeType "httpGet" }}
                httpGet:
                  path: {{ $jobConfig.readinessProbe.path }}
                  scheme: {{ $jobConfig.readinessProbe.scheme }}
                  port: {{ default $.Values.service.internalPort $jobConfig.readinessProbe.port }}
                {{- else if eq $jobConfig.readinessProbe.probeType "tcpSocket" }}
                tcpSocket:
                  port: {{ default $.Values.service.internalPort $jobConfig.readinessProbe.port }}
                {{- else if eq $jobConfig.readinessProbe.probeType "exec" }}
                exec:
                  command:
                    {{ toYaml $jobConfig.readinessProbe.command | nindent 18 }}
                {{- end }}
                initialDelaySeconds: {{ $jobConfig.readinessProbe.initialDelaySeconds }}
                timeoutSeconds: {{ $jobConfig.readinessProbe.timeoutSeconds }}
                failureThreshold: {{ $jobConfig.readinessProbe.failureThreshold }}
                periodSeconds: {{ $jobConfig.readinessProbe.periodSeconds }}
              {{- end }}
              resources:
                {{ toYaml $.Values.resources | nindent 16 }}
              {{- if $jobConfig.extraVolumeMounts }}
              volumeMounts:
              {{ toYaml $jobConfig.extraVolumeMounts | nindent 16 }}
              {{- end }}
{{- end -}}
{{- end -}}
